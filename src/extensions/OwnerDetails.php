<?php
/**
 * User: Carsten
 * Date: 30/01/2019
 * Silverstripe 4.x
 * SiteConfig extension for Owner Details
 */
use SilverStripe\ORM\DataExtension;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\EmailField;

class OwnerDetails extends DataExtension {
    private static $db = array(
        'BusinessName'      => 'Varchar(100)',       // Default Varchar has 32, define (255) if more needed.
        'Address1'          => 'Varchar(255)',
        'Address2'          => 'Varchar(255)',
        'City'              => 'Varchar(100)',
        'Postcode'          => 'Varchar',
        'State'             => 'Varchar(60)',
        'Country'           => 'Varchar(60)',
        'AddressNotes'      => 'Text',              // Could be e.g. used for 2nd address - rarely needed so far
        'Email'             => 'Varchar(100)',
        'Email2'            => 'Varchar(100)',
        'Phone'             => 'Varchar',
        'Freephone'         => 'Varchar',
        'Mobile'            => 'Varchar',
        'Fax'               => 'Varchar',
        'Website'           => 'Varchar',
        'Website2'          => 'Varchar',
        'GPS'               => 'Varchar(100)',
        'GoogleMapsLink'    => 'Text',              // Share link from GMaps to open on GMaps website (VarChar not long enough)
        'GoogleMapsEmbed'   => 'Text',              // Embed URL from GMaps for iframe embedding (without iframe tags)
        'OpeningHours'      => 'Text'
    );

    public function updateCMSFields(FieldList $fields) {
        $fields->addFieldToTab("Root.OwnerDetails",
            new TextField("BusinessName", "Official Business Name")
        );

        $fields->addFieldToTab("Root.OwnerDetails",
            new TextField("Address1", "Address Line 1")
        );

        $fields->addFieldToTab("Root.OwnerDetails",
            new TextField("Address2", "Address Line 2")
        );

        $fields->addFieldToTab("Root.OwnerDetails",
            new TextField("City", "Town/City")
        );

        $fields->addFieldToTab("Root.OwnerDetails",
            new TextField("Postcode", "Postcode")
        );

        $fields->addFieldToTab("Root.OwnerDetails",
            new TextField("State", "State")
        );

        $fields->addFieldToTab("Root.OwnerDetails",
            new TextField("Country", "Country")
        );

        $fields->addFieldToTab("Root.OwnerDetails",
            $adrnotes = new TextareaField("AddressNotes", "Address Notes")
        );
        $adrnotes->setAttribute('placeholder','Notes or additional address');

        $fields->addFieldToTab("Root.OwnerDetails",
            new EmailField("Email", "Email")
        );

        $fields->addFieldToTab("Root.OwnerDetails",
            new EmailField("Email2", "Email2")
        );

        $fields->addFieldToTab("Root.OwnerDetails",
            $phone = new TextField("Phone", "Phone")
        );
        $phone->setAttribute('placeholder','Include country code, e.g.: +64 3 123 456');

        $fields->addFieldToTab("Root.OwnerDetails",
            new TextField("Freephone", "Freephone")
        );

        $fields->addFieldToTab("Root.OwnerDetails",
            $mobile = new TextField("Mobile", "Mobile")
        );
        $mobile->setAttribute('placeholder','Include country code, e.g.: +64 21 123 456 7');

        $fields->addFieldToTab("Root.OwnerDetails",
            $fax = new TextField("Fax", "Fax")
        );
        $fax->setAttribute('placeholder','Include country code, e.g.: +64 3 123 456');

        $fields->addFieldToTab("Root.OwnerDetails",
            $website = new TextField("Website", "Website")
        );
        $website->setAttribute('placeholder','Put your website here if you want, e.g.: https://www.mysite.co.nz');

        $fields->addFieldToTab("Root.OwnerDetails",
            $website2 = new TextField("Website2", "Website")
        );
        $website2->setAttribute('placeholder','Additional Website address, e.g.: https://www.mysite2.co.nz');

        $fields->addFieldToTab("Root.OwnerDetails",
            $gmapsLink = new TextField("GoogleMapsLink", "Google Maps Share Link")
        );
        $gmapsLink->setRightTitle("Share Link to open at GoogleMaps, e.g. https://www.google.co.nz/maps/place/...");

        $fields->addFieldToTab("Root.OwnerDetails",
            $gmapsEmbed = new TextField("GoogleMapsEmbed", "Google Maps Embed URL")
        );
        $gmapsEmbed->setRightTitle("GoogleMaps iframe embed src, e.g. https://www.google.com/maps/embed?...");

        $fields->addFieldToTab("Root.OwnerDetails",
            $gmapsEmbed = new TextField("GPS", "GPS coordinates")
        );

        $fields->addFieldToTab("Root.OwnerDetails",
            $hours = new TextareaField("OpeningHours", "Opening Hours")
        );
        $hours->setAttribute('placeholder','e.g. Monday to Friday: 8am-5pm');
    }
}
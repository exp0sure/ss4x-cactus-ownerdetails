## Cactus Social Media SiteConfig Extension
`SilverStripe 4.x`

An extension for DataExtension to add fields for the Website Owner to have a central place to add owner related information like contact details (phone, email, address), Google maps link and opening hours.

---

### Setting content in CMS
The Owner Details settings will be available in the CMS in 'Settings' (left hand navigation) and then under the tab "Owner Details".

---

### Usage in templates

Set the scope to $SiteConfig and address the variables.

Available variables:

- BusinessName   
- Address1       
- Address2       
- City            
- Postcode       
- State          
- Country        
- Email          
- Phone          
- Freephone      
- Mobile         
- Fax            
- GoogleMapsLink 
- GoogleMapsEmbed
- OpeningHours   

Example:
```
<% with $SiteConfig %>
    <% if $Phone %>
        <i class="fas fa-phone"></i> <a href="tel:{$Phone)">{$Phone}</a>
    <% end_if %>
<% end_with %>
```

( Last updated: 30/01/2019 )